#include <QtTest>
#include <QSignalSpy>
#include "board.h"

class TestBoard : public QObject
{
    Q_OBJECT

public:
    TestBoard()
    {

    }

    ~TestBoard() {}

private slots:
    void boardIsConstructedWithDefaultBorders()
    {
        Board board;
        QCOMPARE(board.getBorders(), QRect(-1, -1, 3, 3));
    }

    void boardIsConstructedWithNonDefaultBorders()
    {
        Board board(QRect(-10, -10, 20, 30));
        QCOMPARE(board.getBorders(), QRect(-10, -10, 20, 30));
    }

    void boardIsConstructedEmpty()
    {
        Board board(QRect(0, 0, 2, 1));
        QCOMPARE(board.getMarkAt(Board::Position(0, 0)), Board::empty);
        QCOMPARE(board.getMarkAt(Board::Position(1, 0)), Board::empty);
    }

    void markIsPutOnTheBoard()
    {
        Board board;
        board.putMark(Board::circle, Board::Position(0, 0));
        QCOMPARE(board.getMarkAt(Board::Position(0, 0)), Board::circle);
        board.putMark(Board::cross, Board::Position(1, 0));
        QCOMPARE(board.getMarkAt(Board::Position(1, 0)), Board::cross);
    }

    void getDistancesFromBordersOnTheBorder()
    {
        Board board;
        QCOMPARE(board.getDistancesFromBorders(Board::Position(-1, 0)), QMargins(0, 1, 2, 1));
    }

    void getDistancesFromBordersInsideBoard()
    {
        Board board;
        QCOMPARE(board.getDistancesFromBorders(Board::Position(0, 0)), QMargins(1, 1, 1, 1));
    }

    void getDistancesFromBordersOutsideOfBoard()
    {
        Board board;
        QCOMPARE(board.getDistancesFromBorders(Board::Position(2, 0)), QMargins(3, 1, 1, 1));
    }
};


QTEST_APPLESS_MAIN(TestBoard)

#include "test_board.moc"
