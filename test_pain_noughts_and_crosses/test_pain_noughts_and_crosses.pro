QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \ 
    test_board.cpp \
    ../pain_noughts_and_crosses/board.cpp

HEADERS += \
    ../pain_noughts_and_crosses/board.h

INCLUDEPATH += $$PWD/../pain_noughts_and_crosses
