#include "boardfield.h"
#include <QPainter>
#include <QApplication>

const BoardFieldSignaller BoardField::signaller;

BoardField::BoardField(QPair<int, int> position)
    : position(position), isCursorChanged(false)
{
    setAcceptedMouseButtons(Qt::LeftButton);
    setAcceptHoverEvents(true);
}

BoardField::~BoardField()
{

}

QRectF BoardField::boundingRect() const
{
    QPointF center = centerInSceneCoordinates(position);
    QPointF from_center_to_corner(sideLength/2.0, sideLength/2.0);
    return QRectF(center - from_center_to_corner, center + from_center_to_corner);
}

void BoardField::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen(Qt::gray);
    pen.setWidth(borderWidth);
    painter->setPen(pen);
    painter->drawRect(boundingRect());
}

void BoardField::addChildCircle()
{
    QRectF rect(boundingRect().adjusted(markMargin, markMargin, -markMargin, -markMargin));
    QGraphicsEllipseItem *circle = new QGraphicsEllipseItem(rect, this);
    QPen pen(Qt::red);
    pen.setWidth(markWidth);
    circle->setPen(pen);
    tryToRestoreCursor();
}

void BoardField::addChildCross()
{
    QRectF rect(boundingRect().adjusted(markMargin, markMargin, -markMargin, -markMargin));
    QGraphicsLineItem *line1 = new QGraphicsLineItem(QLineF(rect.topLeft(), rect.bottomRight()), this);
    QGraphicsLineItem *line2 = new QGraphicsLineItem(QLineF(rect.topRight(), rect.bottomLeft()), this);
    QPen pen(Qt::blue);
    pen.setWidth(markWidth);
    line1->setPen(pen);
    line2->setPen(pen);
    tryToRestoreCursor();
}

QPointF BoardField::centerInSceneCoordinates(QPair<int, int> discrete_coordinates)
{
    return QPointF(discrete_coordinates.first*sideLength, discrete_coordinates.second*sideLength);
}

void BoardField::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    if(childItems().isEmpty())
        signaller.boardFieldPressed(this);
}

void BoardField::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverEnterEvent(event);
    tryToOverrideCursor();
}

void BoardField::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverLeaveEvent(event);
    tryToRestoreCursor();
}

void BoardField::tryToOverrideCursor()
{
    if(childItems().isEmpty())
    {
        isCursorChanged = true;
        QApplication::setOverrideCursor(Qt::PointingHandCursor);
    }
}

void BoardField::tryToRestoreCursor()
{
    if(isCursorChanged)
    {
        QApplication::restoreOverrideCursor();
        isCursorChanged = false;
    }
}
