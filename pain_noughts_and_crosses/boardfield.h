#ifndef BOARDFIELD_H
#define BOARDFIELD_H

#include <QGraphicsItem>
#include <QPair>
#include <QObject>

class BoardFieldSignaller;

class BoardField : public QGraphicsItem
{
public:
    BoardField(QPair<int, int>);
    virtual ~BoardField();
    QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr);
    void addChildCircle();
    void addChildCross();

    const QPair<int, int> position;
    static const BoardFieldSignaller signaller;
    static constexpr qreal sideLength = 30;
    static constexpr int borderWidth = 1;
    static constexpr int markWidth = 3;
    static constexpr int markMargin = 5;

    static QPointF centerInSceneCoordinates(QPair<int, int> discrete_coordinates);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void tryToOverrideCursor();
    void tryToRestoreCursor();

    bool isCursorChanged;
};

class BoardFieldSignaller : public QObject
{
    Q_OBJECT

    signals:
        void boardFieldPressed(BoardField *field) const;
};

#endif // BOARDFIELD_H
