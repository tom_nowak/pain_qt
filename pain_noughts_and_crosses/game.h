#ifndef GAME_H
#define GAME_H

#include "board.h"
#include <QObject>

class Game : public QObject
{
    Q_OBJECT

public:
    using Line = QPair<Board::Position, Board::Position>;
    Game(QObject *parent = nullptr);
    virtual ~Game();
    void putMark(const Board::Position position);
    const Board &getBoard() const;
    Board::Mark getCurrentPlayerMark() const;

signals:
    void boardResized(QList<Board::Position> added_fields);
    void gameOver(QList<Line> winning_lines);

private:
    Board board;
    Board::Mark currentPlayerMark;
    int resizingMargin;
    int marksInLineToWin;

    QMargins getResizingMargins(const Board::Position position);
    void checkIfGameEnded(const Board::Position last_mark_position);
    void checkIfBoardNeedsResizing(const Board::Position last_mark_position);
    void nextPlayerTurn();
    void appendWinningLineInGivenDirection(QList<Line> &winning_lines, const Board::Position start, const Board::Position direction) const;
};

#endif // GAME_H
