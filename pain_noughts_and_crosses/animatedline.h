#ifndef ANIMATEDLINE_H
#define ANIMATEDLINE_H

#include <QGraphicsObject>
#include <QPropertyAnimation>


class AnimatedLine : public QGraphicsObject
{
public:
    AnimatedLine(const QLineF line);
    virtual ~AnimatedLine();
    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr);
    void startAnimation();

    static constexpr int width = 6;
    static constexpr int animationDurationMilliseconds = 1000;

private:
    QLineF line;
    QPropertyAnimation animation;
};

#endif // ANIMATEDLINE_H
