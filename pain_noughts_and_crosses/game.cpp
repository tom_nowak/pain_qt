#include "game.h"
#include <algorithm>
#include <QDebug>

Game::Game(QObject *parent)
    : QObject(parent), board(QRect(-1, -1, 3, 3)), currentPlayerMark(Board::cross), resizingMargin(2), marksInLineToWin(5)
{

}

Game::~Game()
{

}

void Game::putMark(const Board::Position position)
{
    board.putMark(currentPlayerMark, position);
    checkIfGameEnded(position);
    checkIfBoardNeedsResizing(position);
    nextPlayerTurn();
}

const Board &Game::getBoard() const
{
    return board;
}

Board::Mark Game::getCurrentPlayerMark() const
{
    return currentPlayerMark;
}

QMargins Game::getResizingMargins(const Board::Position position)
{
    QMargins margins = board.getDistancesFromBorders(position);
    margins.setLeft(std::max(0, resizingMargin - margins.left()));
    margins.setTop(std::max(0, resizingMargin - margins.top()));
    margins.setRight(std::max(0, resizingMargin - margins.right()));
    margins.setBottom(std::max(0, resizingMargin - margins.bottom()));
    return margins;
}

void Game::checkIfGameEnded(const Board::Position last_mark_position)
{
    QList<Line> winning_lines;
    appendWinningLineInGivenDirection(winning_lines, last_mark_position, Board::Position(1, 0));
    appendWinningLineInGivenDirection(winning_lines, last_mark_position, Board::Position(0, 1));
    appendWinningLineInGivenDirection(winning_lines, last_mark_position, Board::Position(1, 1));
    appendWinningLineInGivenDirection(winning_lines, last_mark_position, Board::Position(1, -1));
    if(!winning_lines.empty())
        emit gameOver(winning_lines);
}

void Game::checkIfBoardNeedsResizing(const Board::Position last_mark_position)
{
    QMargins margins = getResizingMargins(last_mark_position);
    if(!margins.isNull())
    {
        QList<Board::Position> added_fields = board.extendAndReturnAddedFields(margins);
        emit boardResized(added_fields);
    }
}

void Game::nextPlayerTurn()
{
    if(currentPlayerMark == Board::cross)
        currentPlayerMark = Board::circle;
    else
        currentPlayerMark = Board::cross;
}

void Game::appendWinningLineInGivenDirection(QList<Line> &winning_lines, const Board::Position start, const Board::Position direction) const
{
    int n1 = board.findNumberOfSameMarksInGivenDirection(start, direction);
    const Board::Position opposite_direction(-direction.first, -direction.second);
    int n2 = board.findNumberOfSameMarksInGivenDirection(start, opposite_direction);
    int number_of_fields = n1 + n2 - 1;
    if(number_of_fields < marksInLineToWin)
        return;
    Board::Position winning_line_start(start.first + direction.first*(n1-1), start.second + direction.second*(n1-1));
    Board::Position winning_line_end(start.first + opposite_direction.first*(n2-1), start.second + opposite_direction.second*(n2-1));
    winning_lines.append(Line(winning_line_start, winning_line_end));
    return;
}
