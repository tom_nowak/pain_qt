#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "boardfield.h"
#include "game.h"
#include "board.h"
#include "animatedline.h"
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QMessageBox>
#include <QTime>

namespace
{
void wait(int milliseconds)
{
    QTime end = QTime::currentTime().addMSecs(milliseconds);
    while(QTime::currentTime() < end)
        QCoreApplication::processEvents();
}
}

MainWindow::MainWindow(QMainWindow *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), game(new Game(this))
{
    ui->setupUi(this);
    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(ui->gridLayout);
    setCentralWidget(centralWidget);
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    initializeBoardFields();
    updateMovingPlayerLabel();

    connect(game, &Game::boardResized, this, &MainWindow::boardResized);
    connect(&BoardField::signaller, &BoardFieldSignaller::boardFieldPressed, this, &MainWindow::putMarkOnBoardField);
    connect(game, &Game::gameOver, this, &MainWindow::endGame);
    connect(this, &MainWindow::itemAddedAt, this, &MainWindow::pointCursorAt, Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
    delete ui;
}

BoardField *MainWindow::addBoardField(Board::Position position)
{
    BoardField *bf = new BoardField(position);
    scene->addItem(bf);
    return bf;
}

void MainWindow::initializeBoardFields()
{
    const Board &board = game->getBoard();
    QPoint from = board.getBorders().topLeft();
    QPoint to = board.getBorders().bottomRight();
    for(int x = from.x(); x <= to.x(); ++x)
    {
        for(int y = from.y(); y <= to.y(); ++y)
        {
            Board::Position position(x, y);
            BoardField *bf = addBoardField(position);
            Board::Mark mark = board.getMarkAt(position);
            if(mark == Board::circle)
                bf->addChildCircle();
            else if(mark == Board::cross)
                bf->addChildCross();
        }
    }
}

void MainWindow::boardResized(QList<Board::Position> added_positions)
{
    for(Board::Position position : added_positions)
        addBoardField(position);
}

void MainWindow::putMarkOnBoardField(BoardField *field)
{
    if(game->getCurrentPlayerMark() == Board::circle)
        field->addChildCircle();
    else
        field->addChildCross();
    game->putMark(field->position);
    updateMovingPlayerLabel();
    emit itemAddedAt(field->boundingRect().center());
}

void MainWindow::endGame(QList<Game::Line> winning_lines)
{
    for(auto line : winning_lines)
    {
        QPointF line_begin(BoardField::centerInSceneCoordinates(line.first));
        QPointF line_end(BoardField::centerInSceneCoordinates(line.second));
        AnimatedLine *animated_line = new AnimatedLine(QLineF(line_begin, line_end));
        scene->addItem(animated_line);
        animated_line->startAnimation();
    }
    wait(AnimatedLine::animationDurationMilliseconds);
    QMessageBox message_box(this);
    message_box.setText(tr("Game over! Winner: ") + currentPlayerName());
    message_box.exec();
    close();
}

void MainWindow::pointCursorAt(QPointF scene_coordinates)
{
    QPoint view_pos = ui->graphicsView->mapFromScene(scene_coordinates);
    QPoint global_pos = ui->graphicsView->viewport()->mapToGlobal(view_pos);
    QCursor c = cursor();
    c.setPos(global_pos);
    setCursor(c);
}

QString MainWindow::currentPlayerName()
{
    Q_ASSERT(game->getCurrentPlayerMark() != Board::empty);
    if(game->getCurrentPlayerMark() == Board::circle)
        return tr("circles");
    return tr("crosses");
}

void MainWindow::updateMovingPlayerLabel()
{
    ui->labelPlayerMoving->setText(tr("moving: ") + currentPlayerName());
}
