#include "board.h"
#include <QPoint>
#include <QDebug>
#include <algorithm>
#include <cmath>

Board::Board(QRect borders)
    : borders(borders)
{

}

Board::~Board()
{

}

void Board::putMark(const Mark mark, const Position position)
{
    Q_ASSERT(mark != empty);
    Q_ASSERT(!usedFields.contains(position));
    Q_ASSERT(borders.contains(QPoint(position.first, position.second)));
    usedFields.insert(position, mark);
}

Board::Mark Board::getMarkAt(const Position position) const
{
    return usedFields.value(position);
}

QRect Board::getBorders() const
{
    return borders;
}

QMargins Board::getDistancesFromBorders(const Board::Position position) const
{
    return QMargins(
        std::abs(position.first - borders.left()),
        std::abs(position.second - borders.top()),
        std::abs(position.first - borders.right()),
        std::abs(position.second - borders.bottom())
    );
}

QList<Board::Position> Board::extendAndReturnAddedFields(const QMargins margins)
{
    Q_ASSERT(margins.left() >= 0 && margins.top() >= 0 && margins.right() >= 0 && margins.bottom() >= 0);
    Q_ASSERT(!margins.isNull());
    QRect old_borders = borders;
    borders += margins;
    return makeAddedFieldsList(old_borders);
}

int Board::findNumberOfSameMarksInGivenDirection(const Position start, const Position direction) const
{
    const Board::Mark mark = getMarkAt(start);
    Q_ASSERT(mark != Board::empty);
    Board::Position position = start;
    int counter = 0;
    do
    {
        counter += 1;
        position.first += direction.first;
        position.second += direction.second;
    } while(getMarkAt(position) == mark);
    return counter;
}

QList<Board::Position> Board::makeAddedFieldsList(const QRect old_borders) const
{
    QList<Board::Position> retval;
    QRect left(borders.left(), old_borders.top(), old_borders.left() - borders.left(), old_borders.height());
    QRect right(old_borders.right() + 1, old_borders.top(), borders.right() - old_borders.right(), old_borders.height());
    QRect top_with_corners(borders.left(), borders.top(), borders.width(), old_borders.top() - borders.top());
    QRect bottom_with_corners(borders.left(), old_borders.bottom() + 1, borders.width(), borders.bottom() - old_borders.bottom());
    addAreaToFieldsList(retval, left);
    addAreaToFieldsList(retval, right);
    addAreaToFieldsList(retval, top_with_corners);
    addAreaToFieldsList(retval, bottom_with_corners);
    return retval;
}

void Board::addAreaToFieldsList(QList<Board::Position> &list, const QRect area)
{
    for(int x = area.left(); x <= area.right(); ++x)
    {
        for(int y = area.top(); y <= area.bottom(); ++y)
            list.append(Position(x, y));
    }
}
