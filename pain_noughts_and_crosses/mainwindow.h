#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QPair>
#include <QString>
#include "game.h"

class QGraphicsScene;
class Game;
class BoardField;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QMainWindow *parent = nullptr);
    ~MainWindow();

signals:
    void itemAddedAt(QPointF scene_coordinates);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    Game *game;

    BoardField *addBoardField(Board::Position position);
    void initializeBoardFields();
    QString currentPlayerName();
    void updateMovingPlayerLabel();

private slots:
    void boardResized(QList<Board::Position> added_positions);
    void putMarkOnBoardField(BoardField *field);
    void endGame(QList<Game::Line> winning_lines);
    void pointCursorAt(QPointF scene_coordinates);
};

#endif // MAINWINDOW_H
