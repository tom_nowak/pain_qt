#include "animatedline.h"
#include <QPen>
#include <QPainter>
#include <algorithm>

AnimatedLine::AnimatedLine(const QLineF line)
    : line(line), animation(this, "scale")
{
    setTransformOriginPoint(line.p1());
    animation.setDuration(animationDurationMilliseconds);
    animation.setStartValue(0.0f);
    animation.setEndValue(1.0f);
}

AnimatedLine::~AnimatedLine()
{

}

QRectF AnimatedLine::boundingRect() const
{
    QPointF top_left(std::min(line.p1().x(), line.p2().x()), std::min(line.p1().y(), line.p2().y()));
    QPointF bottom_right(std::max(line.p1().x(), line.p2().x()), std::max(line.p1().y(), line.p2().y()));
    return QRectF(top_left, bottom_right);
}

void AnimatedLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen(Qt::green);
    pen.setWidth(width);
    painter->setPen(pen);
    painter->drawLine(line);
}

void AnimatedLine::startAnimation()
{
    animation.start();
}
