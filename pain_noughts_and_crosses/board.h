#ifndef BOARD_H
#define BOARD_H

#include <QHash>
#include <QPair>
#include <QRect>
#include <QList>
#include <QMargins>

class Board
{
public:
    enum Mark { empty = 0, circle = 1, cross = 2 };
    using Position = QPair<int, int>;

    Board(QRect borders = QRect(-1, -1, 3, 3));
    virtual ~Board();
    void putMark(const Mark mark, const Position position);
    Mark getMarkAt(const Position position) const;
    QRect getBorders() const;
    QMargins getDistancesFromBorders(const Position position) const;
    QList<Position> extendAndReturnAddedFields(const QMargins margins);
    int findNumberOfSameMarksInGivenDirection(const Position start, const Position direction) const;

private:
    QHash<QPair<int, int>, Mark> usedFields;
    QRect borders;

    QList<Position> makeAddedFieldsList(const QRect old_borders) const;
    static void addAreaToFieldsList(QList<Position> &list, const QRect area);
};

#endif // BOARD_H
